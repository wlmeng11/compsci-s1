import java.util.*;

public class Randp {
	private ArrayList<Integer> list;

	public Randp(int max) { 
		list = new ArrayList<Integer>(max);
		// fill the list with the natural numbers up to max
		for (int i = 1; i <= max; i++)
			list.add(i);
	}

	public int nextInt() {
		if (list.isEmpty())
			return 0;
		// generate a random index, no greater than the size of the list
		int index = (int)(list.size()*Math.random());
		// "pop" the element at the index
		return list.remove(index);
	}

	public static void main(String[] args) {
		Randp rand = new Randp(15);
		for (int i = 0; i < 20; i++)
			System.out.println(rand.nextInt());

		System.out.println("test again to confirm randomness:");
		rand = new Randp(15);
		for (int i = 0; i < 20; i++)
			System.out.println(rand.nextInt());
	}
}
