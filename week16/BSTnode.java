/**
 * A node class for BST
 * @author William Meng
 * @param	<T extends Comparable<T>>	Can use any data type that can be compared
 */
public class BSTnode<T extends Comparable<T>> {
	private T datum;
	private BSTnode<T> left;
	private BSTnode<T> right;

	public BSTnode(T datum) {
		this.datum = datum;
		left = right = null; // initialize without children
	}

	// Accessors
	T getDatum() { return datum; }
	BSTnode<T> getLeft() { return left; }
	BSTnode<T> getRight() { return right; }

	/** If single-child node, return that child.
	 * Else, return null
	 */
	BSTnode<T> getChild() {
		if (oneChild()) {
			if (left == null)
				return right;
			return left;
		}
		return null;
	}

	public boolean isLeaf() {
		return left == null && right == null;
	}
	public boolean oneChild() {
		return left == null ^ right == null;
	}
	// END accessors

	// Modifiers
	// Create new child node
	public void setLeft(T datum) { left = new BSTnode<T>(datum); }
	public void setRight(T datum) { right = new BSTnode<T>(datum); }

	// Set child as existing node
	public void setLeft(BSTnode<T> left) { this.left = left; }
	public void setRight(BSTnode<T> right) { this.right = right; }

	// Change datum (for deletion with two children)
	public void setDatum(T datum) { this.datum = datum; }
	// END modifiers

	// Utilities
	public void printTree() {
		System.out.println("[" + toString() + "]");
	}
	/** In-order tree traversal */
	public String toString() {
		String ret = datum.toString();
		if (left != null)
			ret = left.toString() + " " + ret;
		if (right != null)
			ret += " " + right.toString();
		return ret;
	}
	// END utilities

	int depth() {
		if (isLeaf()) return 1; // base case: leaf node has depth 1
		// otherwise, get subtree depths (if not null)
		int leftD = 0, rightD = 0;
		if (left != null)
			leftD = left.depth();
		if (right != null)
			rightD = right.depth();
		// return one plus depth of whichever subtree has greater depth
		return (leftD > rightD)? ++leftD : ++rightD;
	}

	/** Return whether node was inserted successfully */
	boolean insert(T datum) {
		if (datum.compareTo(this.datum) > 0) { // greater than current node, so insert right
			if (right == null) { // insert as new node
				setRight(datum);
				return true;
			}
			return right.insert(datum); // or pass on to child
		}
		else if (datum.compareTo(this.datum) < 0) { // less than current node, so insert left
			if (left == null) { // insert as new node
				setLeft(datum);
				return true;
			}
			return left.insert(datum); // or pass on to child
		}
		// duplicate node, not allowed
		return false;

	}

	// EXTRA CREDIT
	/** Return whether node was deleted successfully */
	boolean delete(T datum) {
		// base case
		if (isLeaf()) // This is not the node you're looking for. Move along.
			return false;

		if (left != null && left.getDatum() == datum) {
			// deleting a leaf
			if (left.isLeaf())
				left = null; // poof!

			// deleting a single-child parent
			else if (left.oneChild())
				left = left.getChild(); // replace left node with its child

			// to-be-deleted node has two children
			else {
				T replacement = rightmost(left.getLeft()).getDatum(); // find greatest element less than node, which cannot have 2 children
				left.setDatum(replacement); // copy that value
				left.delete(replacement); // delete that value in subtree
			}
		}
		// and all the leftside code is repeated here because java doesn't have pointers
		else if (right != null && right.getDatum() == datum) {
			// deleting a leaf
			if (right.isLeaf())
				right = null; // poof!

			// deleting a single-child parent
			else if (right.oneChild())
				right = right.getChild(); // replace right node with its child

			// to-be-deleted node has two children
			else {
				T replacement = rightmost(right.getLeft()).getDatum();
				right.setDatum(replacement);
				delete(replacement);
			}
		}
		// target not found here, try to delete in each subtree
		// we already know this is not a leaf node
		else if (left == null)
			return right.delete(datum);
		else if (right == null)
			return left.delete(datum);
		else
			return left.delete(datum) || right.delete(datum); // try both subtrees; return true if one subtree succeeds

		System.gc(); // clean up deleted nodes (only if the JVM is feeling agreeable, of course)
		return true;
	}

	/**
	 * Helper method for delete
	 */
	static <T extends Comparable<T>> BSTnode<T> rightmost(BSTnode<T> self) {
		if (self.getRight() == null) // no right child
			return self;
		else
			return rightmost(self.getRight());
	}
}
