/**
 * A binary search tree with Comparable-bounded generic data type
 * @author William Meng
 * @param	<T extends Comparable<T>>	Can use any data type that can be compared
 */
public class BST<T extends Comparable<T>> {
	private BSTnode<T> root;

	/** Initializes an empty tree */
	public BST() {
		root = null;
	}
	/** Initializes tree with a root node
	 * @param 	datum for root node */
	public BST(T datum) {
		root = new BSTnode<T>(datum);
	}

	// Accessors
	public BSTnode<T> getTree() { return root; }

	public boolean isEmpty() {
		return root == null;
	}
	public boolean isLeaf() {
		if (isEmpty()) return false;
		return root.getLeft() == null && root.getRight() == null;
	}
	// END accessors

	// Utilities
	public void printTree() {
		if (isEmpty()) return; // don't print anything
		root.printTree(); // print root "subtree"
	}
	public String toString() {
		return root.toString();
	}
	// END utilities

	public int depth() {
		// base case
		if (isEmpty()) return 0;
		// otherwise return root depth
		return root.depth();
	}

	/** Return whether node was inserted successfully */
	boolean insert(T datum) {
		if (isEmpty()) {
			root = new BSTnode<T>(datum);
			return true;
		}
		return root.insert(datum);
	}

	// EXTRA CREDIT
	/** Return whether node was deleted successfully */
	boolean delete(T datum) {
		if (isEmpty()) // can't delete nothing
			return false;
		if (root.getDatum() == datum) { // deleting root node
			if (isLeaf())
				root = null;
			else if (root.oneChild())
				root = root.getChild();
			else { // root has 2 children
				T replacement = BSTnode.rightmost(root.getLeft()).getDatum();
				root.setDatum(replacement);
				root.delete(replacement);
			}
			return true;
		}
		return root.delete(datum);
	}

	public static <T extends Comparable<T>> void printDepth(BST<T> tree) {
		System.out.println("depth: " + tree.depth());
	}

	public static void main(String[] args) {
		//average_depth();
		test();
	}

	/** A simple test case for all the operations */
	public static void test() {
		// a small tree for testing
		System.out.println("\ntest tree:");
		BST<Integer> test = new BST<Integer>();
		printDepth(test);

		int[] arr = {5, 6, 22, 2, 4, 1, 5};
		for (int i : arr) {
			System.out.println("\nInserting " + i);
			if (!test.insert(i))
				System.out.println("Couldn't insert: " + i + " is already in tree");
			else {
				test.printTree();
				printDepth(test);
			}
		}
		System.out.println("empty? " + test.isEmpty());

		System.out.println("\n\nTest deletion:");
		arr = new int[] {3, 4, 1, 22, 5, 2, 6};
		// this deletes: (in order)
		// 3, a non-existent node
		// 4, a leaf node
		// 1, another leaf node
		// 22, a leaf node
		// 5, a root node with 2 children
		// 2, a root node with 1 child
		// 6, a root node with no children
		// Debugged until every case works! :)
		for (int i : arr) {
			System.out.println("\nDeleting " + i);
			if (!test.delete(i))
				System.out.println("Couldn't delete: " + i + " not found");
			else {
				if (test.root != null) System.out.println("Root: " + test.root.getDatum());
				test.printTree();
				printDepth(test);
			}
		}
	}

	/** Generate a random tree with 10000 Integers */
	public static BST<Integer> randomTree(int length) {
		BST<Integer> tree = new BST<Integer>();
		Randp rand = new Randp(length);

		for (int i = 0; i < length; i++) {
			int datum = rand.nextInt();
			if (!tree.insert(datum))
				System.out.println("Couldn't insert: " + datum + " is already in tree");
		}
		return tree;
	}
	public static BST<Integer> randomTree() {
		return randomTree(10000);
	}


	/** Records the average depth of a tree
	 * with 10000 elements
	 */
	public static void average_depth() {
		final int trials = 100;
		int sum = 0;
		for (int k = 0; k < trials; k++) {
			sum += randomTree().depth();
		}
		System.out.println("Average depth of tree with 10000 elements: " + (double)sum/trials);
	}
}
