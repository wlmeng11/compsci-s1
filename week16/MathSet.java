import java.util.*;

/**
 * Uses TreeMap<E> as the backing data structure
 * Inherits some methods from AbstractSet<E>
 * Following style of Java standard library implementations of Set
 *
 * @author	William Meng
 * @param	<E extends Comparable<E>>	Can use any data type that can be compared
 */
public class MathSet<E extends Comparable<E>>
	extends AbstractSet<E>
	implements Set<E> {

	private transient TreeMap<E,Object> tree;
	private static final Object DUMMY = new Object(); // dummy object as value in the map

	/**
	 * Creates an empty set
	 */
	public MathSet() {
		tree = new TreeMap<>();
	}

	@Override
	public boolean add(E e) {
		return tree.put(e, DUMMY) == null;
	}

	@Override
	public void clear() {
		tree = new TreeMap<>();
	}

	@Override
	public Iterator<E> iterator() {
		return tree.keySet().iterator();
	}

	@Override
	public boolean remove(Object o) {
		return tree.remove(o) == DUMMY;
	}

	@Override
	public int size() {
		return tree.size();
	}

	/**
	 * Returns the union set of two mathematical sets
	 * @param	first set
	 * @param	second set
	 * @return	union set
	 */
	public static <E extends Comparable<E>>
		MathSet<E> union(MathSet<E> a, MathSet<E> b) {
			MathSet<E> out = new MathSet<>();
			// iterate through a; add all elements to out
			Iterator<E> it = a.iterator();
			for (int i = 0; i < a.size(); i++) {
				out.add(it.next());
			}
			// iterate through b; add all elements to out
			it = b.iterator();
			for (int i = 0; i < b.size(); i++) {
				out.add(it.next());
			}
			return out;
		}

	/**
	 * Returns the intersection set of two mathematical sets
	 * @param	first set
	 * @param	second set
	 * @return	intersection set
	 */
	public static <E extends Comparable<E>>
		MathSet<E> intersect(MathSet<E> a, MathSet<E> b) {
			MathSet<E> out = new MathSet<>();
			// iterate through a; add all elements to out that are also in b
			Iterator<E> it = a.iterator();
			for (int i = 0; i < a.size(); i++) {
				E elem = it.next();
				// if b contains this element, add it to out
				if (b.contains(elem))
					out.add(elem);
			}
			return out;
		}

	/*
	 * A static method implementation makes more sense
	 * in terms of mathematical syntax,
	 * but here are instance method versions.
	 */

	/**
	 * @return	union set
	 */
	public MathSet<E> union(MathSet<E> that) {
		return union(this, that);
	}

	/**
	 * @return	intersection set
	 */
	public MathSet<E> intersect(MathSet<E> that) {
		return intersect(this, that);
	}

	public static void main(String[] args) {
		MathSet<Integer> a = new MathSet<>();
		a.add(1);
		a.add(2);
		a.add(3);
		System.out.println("Set a: " + a);

		MathSet<Integer> b = new MathSet<>();
		b.add(3);
		b.add(4);
		b.add(5);
		System.out.println("Set b: " + b);

		System.out.println("Union of a and b: " + union(a, b));
		System.out.println("Intersection of a and b: " + intersect(a, b));
	}
}
