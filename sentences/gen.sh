#!/bin/bash
# Note the quadruple "\" because they need to be escaped in bash

LASTWEEK=`ls *.tex | sed -e s/\.tex//g | sort -g | tail -1`
WEEK=$(($LASTWEEK+1))
DATE=`date +"%m/%d/%y"`

if [ -z "$1" ]; then
	FILE=$WEEK.tex
else
	FILE=$1
fi

#echo Last week: $LASTWEEK
#echo This week: $WEEK
#echo File: $FILE

if [ -f $FILE ]; then
	echo ERRROR: File $FILE already exists!
	exit
fi

echo "\documentclass[letterpaper]{report}
\usepackage[margin=1.0in]{geometry}
\parindent0pt
\begin{document}
William Meng \\\\
$DATE \\\\
AP CS \\\\
Week $WEEK Sentences

\begin{enumerate}" >> $FILE

for i in 1 2 3 4 5 6
do
	echo -e "\t\\item %-----$i-----\n\t\t%TODO" >> $FILE

	# Begin inner loop
	for ((k = $((6 - $i)); k > 0; k--)); do
		#echo $i and $k
		echo -e "\t\\item\n\t\t%TODO" >> $FILE
	done
	# End inner loop
done

echo "\end{enumerate}
\end{document}" >> $FILE

\gvim $FILE
\pdflatex $FILE
xdg-open $WEEK.pdf &

exit 0
