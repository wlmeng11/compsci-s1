import java.awt.*;

public class Caption extends CowPart {
	String[] caption;
	Caption() {
		caption = new String[] {
			"~ ~ ~ ~ ~ ~ ~ ~ ~ Recompile this if ~ ~ ~ ~ ~ ~ ~ ~ ~",
			"~ ~ ~ ~ you are a beautiful strong spotted cow ~ ~ ~",
			"~ ~ ~ ~ ~ ~ ~ who don't need no blotches ~ ~ ~ ~ ~ ~ ~"
		};

		x = 100;
		y = 500;
	}
	public void draw(Graphics2D cow) {
		cow.setColor(Color.PINK);

		for (String line : caption) {
			cow.drawString(line, x, y);
			y += 20;
		}
	}
}
