import java.awt.*;

public class Grass extends CowPart {

	Grass() {
		x = 0;
		y = 400;
		width = height = 9000;
	}
	Grass(int x, int y, int width, int height) {
		super(x, y, width, height);
	}

	public void draw(Graphics2D cow) {
		cow.setColor(Color.GREEN);
		cow.fillRect(0, y, width, height);
	}
}
