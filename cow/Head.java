import java.awt.*;
import java.awt.geom.*;

public class Head extends CowPart {
	Polygon head;

	Head() {
		head = new Polygon();
		path = new Path2D.Double();
		color = Color.WHITE;

		x = 350;
		y = 50;

		generatePoly();
		path.append(head, false);
		rotatePath(-.2, x, y);
	}

	private void generatePoly() {
		/* Right side */
		head.addPoint(100, 0); // top right
		head.addPoint(117, 25);
		head.addPoint(133, 75);
		head.addPoint(87, 200); // bottom right
		/* Left side */
		head.addPoint(47, 200); // bottom left
		head.addPoint(0, 75);
		head.addPoint(17, 25);
		head.addPoint(33, 0); // top left

		head.translate(x, y);

		Rectangle bounds = head.getBounds();
		width = bounds.width;
		height = bounds.height;
	}

	public void draw(Graphics2D cow) {
		super.draw(cow);
		drawOutline();
	}
}
