import java.awt.*;
import java.awt.geom.*;
import javax.swing.JFrame;

public class CowFrame extends JFrame {
	private Graphics2D cow;

	public CowFrame () {
		init();
	}

	public void init() {
		setSize(700, 600);
		setBackground(Color.CYAN); // bluish sky for background
		repaint();
	}

	public void paint(Graphics gin) {
		this.cow = (Graphics2D) gin;

		/* Instantiate parts of the cow */
		Grass grass = new Grass();
		Body body = new Body();
		Legs legs = new Legs(body, grass);
		Udder udder = new Udder(body, legs);
		Tail tail = new Tail(body);
		Spots spots = new Spots(body);
		Head head = new Head();
		Eyes eyes = new Eyes(head);
		Ears ears = new Ears(head);
		Caption caption = new Caption();
		Sun sun = new Sun();

		/* Create array of all the cow parts */
		CowPart[] cowParts = new CowPart[] {
			grass,
			udder,
			tail,
			body,
			legs,
			spots,
			ears, // draw ears under head
			head,
			eyes,
			caption,
			sun
		};

		/* Draw all cow parts */
		for (CowPart cowPart : cowParts) {
			cowPart.draw(cow);
		}

	}
}
