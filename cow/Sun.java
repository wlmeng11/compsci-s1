import java.awt.*;
import java.awt.geom.*;

public class Sun extends CowPart {
	Sun() {
		width = height = 100;
		x = 550;
		y = 50;
	}
	public void draw(Graphics2D cow) {
		cow.setColor(Color.YELLOW);
		cow.fillOval(x, y, width, height);
	}
}
