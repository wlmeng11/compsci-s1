import java.awt.*;

public class Spots extends CowPart {
	private Body body;
	private int spotDiam;

	Spots(Body body) {
		this.body = body;
		spotDiam = 30;
	}

	public void draw(Graphics2D cow) {
		cow.setColor(Color.BLACK);

		for (int y = body.getY() + 20; y < body.getY() + body.getHeight() - (spotDiam + 20); y += spotDiam + 20) {
			for (int x = body.getX() + 20 + y%30; x < body.getX() + body.getWidth() - (spotDiam + 20); x += 50) {
				cow.fillOval(x, y + x%30, spotDiam, spotDiam);
			}
		}
	}
}
