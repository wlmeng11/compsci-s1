import java.awt.*;

public class Udder extends CowPart {

	Udder(Body body, Legs legs) {
		x = body.getX() + 2*legs.getWidth() + legs.getDist();
		y = body.getY() + body.getHeight() - 50;
		width = 100;
		height = 65;
	}

	public void draw(Graphics2D cow) {
		cow.setColor(Color.PINK);

		cow.fillOval(x, y, width, height);
	}
}
