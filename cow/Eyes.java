import java.awt.*;
import java.awt.geom.*;

public class Eyes extends CowPart {
	Head head;
	Ellipse2D.Double eye1;
	Ellipse2D.Double eye2;

	int xOffset;
	int yOffset;

	Eyes(Head head) {
		super();
		this.head = head;
		color = Color.BLACK;

		x = head.getX();
		y = head.getY();
		xOffset = 50;
		yOffset = 30;
		height = width = 20;

		createEyes();
		matchHeadRotation();
	}
	private void createEyes() {
		eye1 = new Ellipse2D.Double(x + xOffset, y + yOffset, width, height);
		eye2 = new Ellipse2D.Double(x + head.getWidth() - xOffset, head.getY() + yOffset, width, height);
		path.append(eye1, false);
		path.append(eye2, false);
	}
	private void matchHeadRotation() {
		at = head.getTransform();
		path.transform(at);
	}
}
