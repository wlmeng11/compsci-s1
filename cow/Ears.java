import java.awt.*;
import java.awt.geom.*;

public class Ears extends CowPart {
	Head head;

	Path2D ear1;
	Path2D ear2;

	Ears(Head head) {
		this.head = head;
		color = Color.BLACK;
		x = head.getX();
		y = head.getY();
		width = 80;
		height = 30;
		ear1 = new Path2D.Double();
		ear2 = new Path2D.Double();

		ear1.append( new Ellipse2D.Double(x - 30, y, width, height), false);
		ear2.append( new Ellipse2D.Double(x + head.getWidth() - width + 30, y, width, height), false);

		tiltEars();

		path.append(ear1, false);
		path.append(ear2, false);
		matchHeadRotation();
	}

	private void tiltEars() {
		rotatePath(ear1, .2, x-30 + width, y);
		rotatePath(ear2, -.2, x + head.getWidth() - width + 30, y);
	}

	private void matchHeadRotation() {
		at = head.getTransform();
		path.transform(at);
	}
}
