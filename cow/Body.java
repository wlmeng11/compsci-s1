import java.awt.*;

public class Body extends CowPart {
	private int cornerWidth;
	private int cornerHeight;

	Body() {
		x = 50;
		y = 100;
		width = 400;
		height = 200;
		cornerWidth = 50;
		cornerHeight = 50;
	}
	public int getCornerWidth() { return cornerWidth; }
	public int getCornerHeight() { return cornerHeight; }

	public void draw(Graphics2D cow) {
		cow.setColor(Color.WHITE);
		cow.fillRoundRect(x, y, width, height, cornerWidth, cornerHeight);
	}
}
