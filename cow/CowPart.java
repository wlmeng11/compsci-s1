import java.awt.*;
import java.awt.geom.*;

public abstract class CowPart {
	int x;
	int y;
	int width;
	int height;
	double theta;

	Graphics2D cow;
	Color color;

	AffineTransform at;
	Path2D path;

	CowPart() {
		path = new Path2D.Double();
	}
	CowPart(int x, int y, int width, int height) {
		this();

		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	/* Getter methods */
	public final int getX() { return x; }
	public final int getY() { return y; }
	public final int getWidth() { return width; }
	public final int getHeight() { return height; }
	public final AffineTransform getTransform() { return at; }
	public final Path2D getPath() { return path; }

	/* Rotate path around pivot point (x,y) by theta (radians) */
	public void rotatePath(double theta, int x, int y) {
		this.theta = theta;
		at = AffineTransform.getRotateInstance(theta, x, y);
		path.transform(at);
	}
	/* Receive and return path instead */
	public Path2D rotatePath(Path2D path, double theta, int x, int y) {
		this.theta = theta;
		at = AffineTransform.getRotateInstance(theta, x, y);
		path.transform(at);

		return path;
	}

	public void draw(Graphics2D cow) {
		this.cow = cow;

		cow.setColor(color);
		cow.fill(path);
	}
	public void drawOutline() {
		cow.setColor(Color.BLACK);
		cow.draw(path);
	}
}
