import java.awt.*;

public class Legs extends CowPart {
	Body body;
	private int dist; // distance between legs
	private int length;

	public int getDist() { return dist; }

	Legs(Body body, Grass grass) {
		this.body = body;
		x = body.getX();
		dist = 30;
		y = body.getY() + body.getHeight() - body.getCornerHeight();
		width = 40;
		length = grass.getY() - y;
	}

	public void draw(Graphics2D cow) {
		cow.setColor(Color.WHITE);

		cow.fillRect(x, y, width, length); // leg 1
		cow.fillRect(x + width + dist, y, width, length); // leg 2
		cow.fillRect(x + body.getWidth() - 2 * width - dist, y, width, length); // leg 3
		cow.fillRect(x + body.getWidth() - width, y, width, length); // leg 4
	}
}
