import java.awt.*;
import java.awt.geom.*;

public class Tail extends CowPart {
	Body body;
	RoundRectangle2D tail;

	Tail(Body body) {
		color = Color.WHITE;
		x = body.getX() + 7;
		y = body.getY();
		width = 20;
		height = 175;

		tail = new RoundRectangle2D.Double(x, y, width, height, width, width);

		path.append(tail, false);
		rotatePath(.3, x, y);
	}
}
