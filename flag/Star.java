import java.awt.*;
import java.awt.geom.*;

/**
 * Creates a Polygon object containing an n-pointed star where n>=2,
 * centered at point (x, y).
 */
public class Star {
	private	Polygon star;
	private	int x, y, diameter;
	private	int nLegs = 5;
	private	double rInner, rOuter;
	private	int[] xPoints, yPoints;
	private	double theta;

	Star() {
		this(6, 100, 100, 200);
	}
	Star(int x, int y, int diameter) {
		this(5, x, y, diameter);
	}
	Star(int nLegs, int x, int y, int diameter) {
		this.nLegs = nLegs;
		this.x = x;
		this.y = y;
		this.diameter = diameter;
		rOuter = diameter/2;

		setShape();
	}

	private void setShape() {
		switch(nLegs) {
			case 3:
				rInner = rOuter/3;
				break;
			case 4:
				rInner = diameter/5;
				break;
			case 5:
				rInner = rOuter * Math.sqrt(.1*(25.0-11.0* Math.sqrt(5.0) )) / Math.sqrt(.1*(5.0-Math.sqrt(5.0))); // Source: http://mathworld.wolfram.com/Pentagram.html
				break;
			case 6:
				rInner = rOuter * Math.sqrt(3.0)/3; // Source: http://mathworld.wolfram.com/Hexagram.html
				break;
			default:
				rInner = rOuter/2;
				break;
		}

		xPoints = new int[nLegs*2];
		yPoints = new int[nLegs*2];
		theta = -Math.PI/2; // Start with point facing up

		for (int i = 0; i < nLegs*2; i++) {
			/* Outer points */
			xPoints[i] = (int)(rOuter * Math.cos(theta));
			yPoints[i] = (int)(rOuter * Math.sin(theta));
			i++;
			theta += Math.PI/nLegs;
			/* Inner points */
			xPoints[i] = (int)(rInner * Math.cos(theta));
			yPoints[i] = (int)(rInner * Math.sin(theta));
			theta += Math.PI/nLegs;
		}

		star = new Polygon(xPoints, yPoints, nLegs*2);
		star.translate(x, y);
	}

	public Polygon getShape() {
		return star;
	}

}
