import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.*;

/* Create standard sized everything
 * Resize everything with an AffineTransform
 * Draw everything
 */

public class FlagFrame extends JFrame implements ComponentListener, MouseListener {
	private final Color OLD_GLORY_BLUE = new Color(0x00214d);
	private final Color OLD_GLORY_RED = new Color(0xC1133D);

	/**
	 * Provides common fields and methods for transforming and drawing for each part of the flag.
	 */
	private abstract class FlagPart {
		Graphics2D flag;
		Color color; // set this in subclass constructors
		Shape shape;
		Path2D path;
		Path2D scaledPath;
		double x, y, width, height;
		boolean antiAlias = false; /* Draw with anti-aliasing a la Graphics2D library */

		FlagPart() {
			path = new Path2D.Double(); // original, full sized path (never actually drawn)
			scaledPath = (Path2D.Double)path.clone(); // scaled according to window size and drawn
		}
		public final void transform(AffineTransform at) {
			scaledPath = (Path2D.Double)path.clone(); // first clone the original path
			scaledPath.transform(at); // then transform scaledPath
		}
		public final void draw(Graphics gin) {
			flag = (Graphics2D)gin;
			if (antiAlias) flag.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			flag.setColor(color);
			flag.fill(scaledPath);
		}
		public final void toggleAA() {
			antiAlias ^= true;
			System.out.println("Toggling AA for " + getClass() + ": " + antiAlias);
		}
	}

	/**
	 * Creates 50 stars, arranged in 11 columns
	 * alternating between 5 stars each and 4 stars each.
	 */
	private final class Stars extends FlagPart {
		private int numStars, nLegs;
		private int starX, starY;
		private int starDiam;

		Stars(double flagHeight) {
			super();
			antiAlias = true;
			color = Color.WHITE;
			width = (0.76 * flagHeight);
			height = flagHeight * 7/13;
			x = width/12; // center of top-left star, since stars are drawn from center (0,0)
			y = height/10;

			numStars = 50;
			starX = (int) x;
			starY = (int) y;
			nLegs = 5;
			starDiam = (int)(.0616 * flagHeight);

			createStars();
		}

		public void addLeg() {
			System.out.println("Adding a leg");
			nLegs++; // UNLIMITED INCREMENTS!!! (at least until the maximum integer value)
			createStars();
		}
		public void rmLeg() {
			if (nLegs>2) { // decrement leg to a minimum of 2
				System.out.println("Removing a leg");
				nLegs--;
				createStars();
			}
		}
		public void resetLegs() {
			if (nLegs != 5) {
				System.out.println("Back to 5 legs");
				nLegs = 5;
				createStars();
			}
		}

		void createStars() {
			path = new Path2D.Double(); // reset the Path2D object
			starX = (int) x;
			starY = (int) y;

			for (int i = 1; i <= numStars; i++) {
				path.append(new Star(nLegs, starX, starY, starDiam).getShape(), false);

				/* Branch if the end of a big column has been reached */
				if (i%9 == 5) {
					starX+=(int)x;
					starY=2*(int)y;
				}
				/* Branch if the end of a small column has been reached */
				else if (i%9 == 0) { // bottom of small col reached
					starX+=(int)x;
					starY=(int)y;
				}
				/* Advance to next star down */
				else
					starY+=2*(int)y;
			}
		}
	}

	private final class StarBackground extends FlagPart {
		StarBackground(double flagHeight) {
			super();
			color = OLD_GLORY_BLUE;
			width = 0.76 * flagHeight;
			height = flagHeight * 7/13;
			x = y = 0;
			shape = new Rectangle2D.Double(x, y, width, height);
			path.append(shape, false);
		}
	}

	private final class Stripes extends FlagPart {
		Stripes(double flagHeight) {
			super();
			color = OLD_GLORY_RED;
			width = 1.9 * flagHeight;
			height = flagHeight/13;
			x = y = 0;
			while (flagHeight >= y + height) {
				shape = new Rectangle2D.Double(x, y, width, height);
				path.append(shape, false);
				y += 2*height;
			}
		}
	}

	private final class Background extends FlagPart {
		Background(double flagHeight) {
			super();
			color = Color.WHITE;
			width = 1.9 * flagHeight;
			height = flagHeight;
			x = y = 0;
			shape = new Rectangle2D.Double(x, y, width, height);
			path.append(shape, false);
		}
	}

	private FlagPart[] parts;
	private double width, height;
	private boolean preserveAspectRatio = true;
	private boolean bufferEnabled = true;
	private Stars stars;

	private Action spaceAction = new AbstractAction() {
		public void actionPerformed(ActionEvent e) {
			toggleAspectRatio();
		}
	};
	private Action enterAction = new AbstractAction() {
		public void actionPerformed(ActionEvent e) {
			System.out.println("Frame buffer: " + (bufferEnabled ^= true));
		}
	};
	private Action insertAction = new AbstractAction() {
		public void actionPerformed(ActionEvent e) {
			stars.toggleAA();
			updateContainer();
		}
	};

	FlagFrame() {
		height = 1000;
		width = 1.9 * height;

		/* Key Bindings */
		getRootPane().getInputMap().put(KeyStroke.getKeyStroke("SPACE"), "spaceAction");
		getRootPane().getActionMap().put("spaceAction", spaceAction);
		getRootPane().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "enterAction");
		getRootPane().getActionMap().put("enterAction", enterAction);
		getRootPane().getInputMap().put(KeyStroke.getKeyStroke("INSERT"), "insertAction");
		getRootPane().getActionMap().put("insertAction", insertAction);

		init();
		addComponentListener(this);
		addMouseListener(this);

	}

	public void init() {
		setTitle("William Meng's US Flag");
		setSize(720, 400);
		setExtendedState(this.getExtendedState()|JFrame.MAXIMIZED_BOTH); // maximize by default (if allowed by window manager)

		/* Create the parts of the flag */
		parts = new FlagPart[] {
			new Background(height),
				new Stripes(height),
				new StarBackground(height),
				stars = new Stars(height)
		};

		setVisible(true);
		createBufferStrategy(2); // double buffer

		fitToWindow();
	}

	private void fitToWindow() {
		int windowWidth = getContentPane().getWidth();
		int windowHeight = getContentPane().getHeight();

		/* Scale the flag */
		if (preserveAspectRatio) {
			if (1.9*windowHeight >= windowWidth)
				setScale(windowWidth/width); // fit based on window width
			else
				setScale(windowHeight/height); // fit based on window height
		}
		else
			setScale(windowWidth/width, windowHeight/height); // fit window without maintaining aspect ratio

		/* Redraw the flag */
		updateContainer();
	}

	public void componentHidden(ComponentEvent e) {}
	public void componentMoved(ComponentEvent e) {}
	public void componentShown(ComponentEvent e) {}
	public void componentResized(ComponentEvent e) {
		fitToWindow();
	}

	public void mouseClicked(MouseEvent e) {
		switch(e.getButton()) {
			case MouseEvent.BUTTON1: // left click
				stars.addLeg();
				fitToWindow();
				break;
			case MouseEvent.BUTTON2: // middle click (obviously need a mouse with middle click)
				toggleAspectRatio();
				break;
			case MouseEvent.BUTTON3: // right click
				if (e.getClickCount() >= 3) // triple right click
					stars.resetLegs(); // back to 5 legs
				else
					stars.rmLeg();
				fitToWindow();
				break;
		}
	}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}

	private void toggleAspectRatio() {
		System.out.println("Preserve aspect ratio: " + (preserveAspectRatio ^= true)); // toggle boolean
		fitToWindow();
	}

	/* Resize while maintaining aspect ratio */
	private void setScale(double scale) {
		setScale(scale, scale);
	}
	/* Ignore aspect ratio */
	private void setScale(double xScale, double yScale) {
		for (FlagPart part : parts) {
			part.transform(AffineTransform.getScaleInstance(xScale, yScale));
		}
	}

	public void paint(Graphics g) {
		((Graphics2D)g).translate(getInsets().left, getInsets().top); // translate to compensate for window insets

		/* Draw background */
		g.fillRect(0, 0, getContentPane().getWidth(), getContentPane().getHeight());

		for (FlagPart part : parts) {
			part.draw(g);
		}
	}

	/* Method for frame buffering */
	public void updateContainer() {
		if (bufferEnabled) {
			BufferStrategy bs = getBufferStrategy();
			Graphics g2d = (Graphics2D)bs.getDrawGraphics();
			paint(g2d);
			bs.show();
		}
		else
			repaint();
	}

	public static void main(String[] args) {
		FlagFrame f = new FlagFrame();
		f.setDefaultCloseOperation(f.EXIT_ON_CLOSE);
	}
}
