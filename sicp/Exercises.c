#include <stdio.h>
#include <string.h>
#include <time.h>

char func[80];

/* Exercise 1.11 */
int f_recursive(int n) {
	strcpy(func, __func__);
	if (n < 3)
		return n;
	else
		return f_recursive(n-1) + 2*f_recursive(n-2) + 3*f_recursive(n-3);
}

int f_tailrec(int n) {
	strcpy(func, __func__);
	int f_iter(int a, int b, int c, int counter);
	return f_iter(2, 1, 0, n);
}
// helper function for f_tailrec
int f_iter(int a, int b, int c, int counter) {
	if (counter == 0)
		return c;
	else
		return f_iter(a + 2*b + 3*c, a, b, counter - 1);
}

int f_looping(int n) {
	strcpy(func, __func__);
	int a = 2; // f(n+2)
	int b = 1; // f(n=1)
	int c = 0; // f(n)

	int i;
	for (i = n; i > 0; i--) {
		int tmp = a + 2*b + 3*c;
		c = b; // this must be before b = a
		b = a;
		a = tmp;
	}
	return c;
}

/* Prints the first n results of a function f */
void print_n(int (*f)(int), int n) {
	f(1); puts(func); // print the function name of f (hackishly)

	int i;
	for (i = 1; i <= n; i++) {
		printf("%d ", f(i));
	}
	puts("\n");
}

/* Returns difference in nanoseconds between a start and end struct timespec */
long nanodifftime(struct timespec *start, struct timespec *end) {
	return ((end->tv_sec - start->tv_sec)*1000000000
		+ (end->tv_nsec - start->tv_nsec));
}

/* Print system runtime, in ns, for each function call with increasing i to n */
void time_n(int (*f)(int), int n) {
	f(1); puts(func);
	struct timespec start, end;
	long sum = 0;
	int trials = 20;
	int i, j;
	for (i = 1; i <= n; i++) {
		for (j = 0; j < trials; j++) { // multiple trials
			clock_gettime(CLOCK_REALTIME, &start);
			f(i); // compute
			clock_gettime(CLOCK_REALTIME, &end);
			sum += nanodifftime(&start, &end);
		}
		printf("%d\t%d\n", i, sum/trials);
	}
}

/* Exercise 1.12 */
int pascal(int n, int r) {
	if (n <= 1 || r == 0 || r == n)
		return 1;
	else
		return pascal(n-1, r) + pascal (n-1, r-1);
}

int* pascalrow(int n) {
	int row[n+1];
	int *p = row;
	int r;
	for (r = 0; r <= n; r++) {
		row[r] = pascal(n, r);
	}
	return p;
}

int main(int argc, char const *argv[]) {
	int n = 5;
	print_n(f_recursive, n);
	print_n(f_tailrec, n);
	print_n(f_looping, n);

	n = 30;
	time_n(f_recursive, n);
	n = 500;
	time_n(f_tailrec, n);
	time_n(f_looping, n);

	n = 5;
	int *row;
	row = pascalrow(n);
	//n = sizeof(row)/sizeof(*row); // number of elements DOESN'T WORK FOR decayed arrays
	int i;
	for (i = 0; i < n+1; i++)
		printf("%d ", *row++);
	puts("");

	return 0;
}
