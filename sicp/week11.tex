% vim: set expandtab:
%        File: week11.tex
%     Created: Tue Oct 15 08:00 PM 2013 P
% Last Change: Tue Oct 15 08:00 PM 2013 P
%
\documentclass[letterpaper]{report}
\usepackage[margin=1.0in]{geometry}
\parindent0pt
\usepackage{minted}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    urlcolor=blue,
    %pdfborderstyle={/S/U/W 1}
}
\usepackage{graphicx}
\begin{document}
William Meng \\
10/15/13 \\
AP CS \\
SICP Exercises 1.11 + 1.12 \\

I intially completed the exercises in Scheme, in keeping with the style of the text, and subsequently ported to Java.
Since Java doesn't have function pointers, I also implemented a C version.
\subsection*{1.11}
\subsubsection*{Using Recursion}
The given mathematical function is already defined recursively, so translating to Java is trivial.
\begin{minted}{java}
static int f_recursive(int n) {
    if (n < 3)
        return n;
    else
        return f_recursive(n-1) + 2*f_recursive(n-2) + 3*f_recursive(n-3);
}
\end{minted}

By measuring execution time of the function in nanoseconds for increasing values of $n$, I generated
data represented in the following graph: \\
\includegraphics{data/rec.png}
This function appears to have an exponential order of growth.

\subsubsection*{An Iterative Process}
We can initialize 3 integers $a$, $b$, and $c$ as
$f(2) = 2$, $f(1) = 1$, and $f(0) = 0$, respectively.

The values of these three variables such that $a = f(n+2)$, $b = f(n+1)$, and $c = f(n)$
can be found by applying the following transformation $n-2$ times:
\[a \leftarrow a + 2b + 3c \]
\[b \leftarrow a \]
\[c \leftarrow b \]

This iterative process can be implemented through either a tail-recursive procedure:
\begin{minted}{java}
static int f_tailrec(int n) {
    return f_iter(2, 1, 0, n);
}
/**
 * Private helper method because Java doesn't have nested functions
 * @param a         f(n+2)
 * @param b         f(n+1)
 * @param c         f(n)
 * @param counter   Counts down until 0, represents n
 */
private static int f_iter(int a, int b, int c, int counter) {
    if (counter == 0)
        return c;
    else
        return f_iter(a + 2*b + 3*c, a, b, counter - 1);
}
\end{minted}

or a looping construct:
\begin{minted}{java}
static int f_looping(int n) {
    int a = 2; // f(n+2)
    int b = 1; // f(n+1)
    int c = 0; // f(n)
    for (int i = 0; i < n; i++) {
        int new_a = a + 2*b + 3*c;
        c = b;
        b = a;
        a = new_a;
    }
    return c;
}
\end{minted}
Java does not have tail recursion optimization, so the looping implementation should be
faster than tail recursion. Scheme, however, does have tail recursion optimization, so
using loops explicitly for optimization purposes is not necessary.

\break
The tail recursive implementation has the following growth model: \\
\includegraphics{data/tailrec.png}
The order of growth appears to be either log-linear or polynomial.
However, notice that the order of growth decreases sharply at around $n=40$, but appears
to have the same shape.
This change is most likely due to Java's JIT compiler, which compiles a method into a
better performing, natively executable binary during runtime once its call count
has reached a certain threshold.

\break
The looping implementation has the following growth model: \\
\includegraphics{data/loop.png}
Once again, the order of growth appears to be either log-linear or polynomial. The sharp
decrease at $n=100$ appears to scale the graph vertically by a fractional amount, and is
likely attributable to JIT compilation.

\break
\subsection*{1.12}
I chose to use 0-indexing both horizontally and vertically to make it syntactically synonmyous
to a combination, $n \choose r$:
\begin{minted}{java}
static int pascal(int n, int r) {
    if (n == 0 && r == 0)
        return 1; // base case: element (0, 0)
    else if (n < 0 || r < 0 || r > n)
        return 0; // fill outside area with zeros
    else
        return pascal(n-1, r) + pascal(n-1, r-1);
}
\end{minted}
The function returns $0$ for any element outside the ``standard'' boundaries of Pascal's triangle. \\

The following function returns the $n^{th}$ row of Pascal's triangle as an array:
\begin{minted}{java}
static int[] pascalrow(int n) {
    int[] row = new int[n+1];
    for (int r = 0; r <= n; r++)
        row[r] = pascal(n, r);
    return row;
}
\end{minted}

\break
\subsection*{Extra Stuff}
Here are the 3 graphs from 1.11 again, but for the C versions: \\
\includegraphics{data/c/rec.png} \\
Interestingly, these graphs indicate that the Java version performed better than C,
which is unlikely to be true in reality.
According to documentation for time measurement functions in both C and Java,
the functions are capable of nanosecond precision, but are not necessarily accurate compared to realtime.

\includegraphics{data/c/tailrec.png} \\
The ``hitch'' in the Java version is not present here; because C is already compiled to a native executable,
the performance should not change during runtime.

\includegraphics{data/c/loop.png} \\
Here, the looping version clearly performs better than the tail recursive version, indicating that the program
was likely compiled without tail recursion optimization.

This is supported by the assembly code for the \verb|f_iter| function:
\begin{minted}{nasm}
f_iter:
.LFB2:
    .cfi_startproc
    pushq   %rbp
    .cfi_def_cfa_offset 16
    .cfi_offset 6, -16
    movq    %rsp, %rbp
    .cfi_def_cfa_register 6
    subq    $16, %rsp
    movl    %edi, -4(%rbp)
    movl    %esi, -8(%rbp)
    movl    %edx, -12(%rbp)
    movl    %ecx, -16(%rbp)
    cmpl    $0, -16(%rbp)
    jne .L7
    movl    -12(%rbp), %eax
    jmp .L8
.L7:
    movl    -16(%rbp), %eax
    leal    -1(%rax), %ecx
    movl    -8(%rbp), %eax
    leal    (%rax,%rax), %edx
    movl    -4(%rbp), %eax
    leal    (%rdx,%rax), %esi
    movl    -12(%rbp), %edx
    movl    %edx, %eax
    addl    %eax, %eax
    addl    %edx, %eax
    leal    (%rsi,%rax), %edi
    movl    -8(%rbp), %edx
    movl    -4(%rbp), %eax
    movl    %eax, %esi
    call    f_iter
.L8:
    leave
    .cfi_def_cfa 7, 8
    ret
    .cfi_endproc
.LFE2:
    .size   f_iter, .-f_iter
    .globl  f_looping
    .type   f_looping, @function
\end{minted}
The \verb|call| instruction and the end of \verb|.L7| indicates a recursive function call. \\

Compiling the program with the \verb|-O2| flag in \verb|gcc| produces the following assembly for the \verb|f_iter| function:
\begin{minted}{nasm}
f_iter:
.LFB27:
    .cfi_startproc
    testl   %ecx, %ecx
    movl    %edx, %eax
    jne .L25
    jmp .L24
    .p2align 4,,10
    .p2align 3
.L30:
    movl    %edi, %esi
    movl    %edx, %edi
.L25:
    leal    (%rax,%rax,2), %eax
    leal    (%rdi,%rsi,2), %edx
    addl    %eax, %edx
    subl    $1, %ecx
    movl    %esi, %eax
    jne .L30
.L24:
    rep ret
    .cfi_endproc
.LFE27:
    .size   f_iter, .-f_iter
    .section    .rodata.str1.1,"aMS",@progbits,1
.LC0:
    .string "\n"
.LC1:
    .string "%d "
    .text
    .p2align 4,,15
    .globl  print_n
    .type   print_n, @function
\end{minted}
Notice that this version does not contain any \verb|call| instructions, but does contain \verb|jne .L30|,
which would cause the program to jump backwards in the function, indicating the presence of a loop
that did not exist in the original C code. \\

Graphs for the optimized C program: \\
\includegraphics{data/c/opt/rec.png} \\
\includegraphics{data/c/opt/tailrec.png} \\
Notice that this optimized C function has greatly reduced runtimes compared to both Java and unoptimized C versions.

\includegraphics{data/c/opt/loop.png} \\
Notice that the performance for the optimized tail recursive and looping functions are nearly identical.
This is further supported by how similar the \verb|f_iter| assembly is to the \verb|f-looping| assembly:
\begin{minted}{nasm}
f_looping:
.LFB28:
    .cfi_startproc
    movabsq $7956013840639745894, %rax
    movl    $103, %edx
    testl   %edi, %edi
    movq    %rax, func(%rip)
    movw    %dx, func+8(%rip)
    jle .L18
    xorl    %edx, %edx
    movl    $1, %eax
    movl    $2, %ecx
    jmp .L17
    .p2align 4,,10
    .p2align 3
.L22:
    movl    %eax, %edx
    movl    %ecx, %eax
    movl    %esi, %ecx
.L17:
    leal    (%rcx,%rax,2), %esi
    leal    (%rdx,%rdx,2), %edx
    addl    %edx, %esi
    subl    $1, %edi
    jne .L22
    rep ret
.L18:
    xorl    %eax, %eax
    ret
    .cfi_endproc
.LFE28:
    .size   f_looping, .-f_looping
    .p2align 4,,15
    .globl  f_iter
    .type   f_iter, @function
\end{minted}


\vfill
Syntax highlighting of code uses the \verb|minted| package for \LaTeX: \\
\underline{\url{https://github.com/gpoore/minted}}
\end{document}
