;; SICP Exercise 1.11
(list "Exercise 1.11")
(define (f-recursive n)
  (if (< n 3)
	  n
	  (+ (f-recursive (- n 1))
		 (* 2 (f-recursive (- n 2)))
		 (* 3 (f-recursive (- n 3))))
	  )
  )

;; iterative version using four state variables
;; and tail recursion
(define (f-iterative n)
  (define (iter a b c counter)
	(if (= counter 0)
		c ; base case
		(iter (+ a (* 2 b) (* 3 c)) a b (- counter 1))
		)
	)
	(iter 2 1 0 n)
  )

(define (f n)
  (f-iter 2 1 0 n))

(define (f-iter a b c count)
  (if (= count 0)
      b
      (f-iter (+ a b) a b (- count 1))))

(list
 (f-recursive 1)
 (f-recursive 2)
 (f-recursive 3)
 (f-recursive 4)
 (f-recursive 5)
 )

(list
 (f-iterative 1)
 (f-iterative 2)
 (f-iterative 3)
 (f-iterative 4)
 (f-iterative 5)
 )

;(list (f 1) (f 2) (f 3) )

;; SICP Exercise 1.12
(list "Exercise 1.12")
; n is row
; r is column, 0 index
(define (pascal n r)
  (if (or (<= n 1) (= r 0) (= r n))
          1 ; return 1
          (+ (pascal (- n 1) r)
             (pascal (- n 1) (- r 1)))))

(pascal 4 2)

;; evaluate the nth row of pascal's triangle
(define (pascalrow n)
  (define (iter n r)
    (if (= n r)
        (list (pascal n 0)) ; rightmost element
        (cons (pascal n r) (iter n (+ r 1)))))
  (iter n 0))

(pascalrow 5)

;; evaluate the first n rows of pascal's triangle
(define (pascalrows n)
  (define (iter n counter)
    (if (= n counter)
        (cons (pascalrow n) (list) ) ; bottom element
        (cons (pascalrow counter) (iter n (+ counter 1)))))
  (iter n 0))

(pascalrows 5)
