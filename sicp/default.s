	.file	"Exercises.c"
	.comm	func,80,32
	.text
	.globl	f_recursive
	.type	f_recursive, @function
f_recursive:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movl	%edi, -20(%rbp)
	movabsq	$8318840531470933862, %rax
	movq	%rax, func(%rip)
	movl	$6649449, func+8(%rip)
	cmpl	$2, -20(%rbp)
	jg	.L2
	movl	-20(%rbp), %eax
	jmp	.L3
.L2:
	movl	-20(%rbp), %eax
	subl	$1, %eax
	movl	%eax, %edi
	call	f_recursive
	movl	%eax, %ebx
	movl	-20(%rbp), %eax
	subl	$2, %eax
	movl	%eax, %edi
	call	f_recursive
	addl	%eax, %eax
	addl	%eax, %ebx
	movl	-20(%rbp), %eax
	subl	$3, %eax
	movl	%eax, %edi
	call	f_recursive
	movl	%eax, %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%ebx, %eax
.L3:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	f_recursive, .-f_recursive
	.globl	f_tailrec
	.type	f_tailrec, @function
f_tailrec:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movabsq	$7310024345038118758, %rax
	movq	%rax, func(%rip)
	movw	$99, func+8(%rip)
	movl	-4(%rbp), %eax
	movl	%eax, %ecx
	movl	$0, %edx
	movl	$1, %esi
	movl	$2, %edi
	call	f_iter
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	f_tailrec, .-f_tailrec
	.globl	f_iter
	.type	f_iter, @function
f_iter:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	%edx, -12(%rbp)
	movl	%ecx, -16(%rbp)
	cmpl	$0, -16(%rbp)
	jne	.L7
	movl	-12(%rbp), %eax
	jmp	.L8
.L7:
	movl	-16(%rbp), %eax
	leal	-1(%rax), %ecx
	movl	-8(%rbp), %eax
	leal	(%rax,%rax), %edx
	movl	-4(%rbp), %eax
	leal	(%rdx,%rax), %esi
	movl	-12(%rbp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	leal	(%rsi,%rax), %edi
	movl	-8(%rbp), %edx
	movl	-4(%rbp), %eax
	movl	%eax, %esi
	call	f_iter
.L8:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	f_iter, .-f_iter
	.globl	f_looping
	.type	f_looping, @function
f_looping:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -36(%rbp)
	movabsq	$7956013840639745894, %rax
	movq	%rax, func(%rip)
	movw	$103, func+8(%rip)
	movl	$2, -4(%rbp)
	movl	$1, -8(%rbp)
	movl	$0, -12(%rbp)
	movl	-36(%rbp), %eax
	movl	%eax, -16(%rbp)
	jmp	.L10
.L11:
	movl	-8(%rbp), %eax
	leal	(%rax,%rax), %edx
	movl	-4(%rbp), %eax
	leal	(%rdx,%rax), %ecx
	movl	-12(%rbp), %edx
	movl	%edx, %eax
	addl	%eax, %eax
	addl	%edx, %eax
	addl	%ecx, %eax
	movl	%eax, -20(%rbp)
	movl	-8(%rbp), %eax
	movl	%eax, -12(%rbp)
	movl	-4(%rbp), %eax
	movl	%eax, -8(%rbp)
	movl	-20(%rbp), %eax
	movl	%eax, -4(%rbp)
	subl	$1, -16(%rbp)
.L10:
	cmpl	$0, -16(%rbp)
	jg	.L11
	movl	-12(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	f_looping, .-f_looping
	.section	.rodata
.LC0:
	.string	"%d "
.LC1:
	.string	"\n"
	.text
	.globl	print_n
	.type	print_n, @function
print_n:
.LFB4:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movq	-24(%rbp), %rax
	movl	$1, %edi
	call	*%rax
	movl	$func, %edi
	call	puts
	movl	$1, -4(%rbp)
	jmp	.L14
.L15:
	movl	-4(%rbp), %edx
	movq	-24(%rbp), %rax
	movl	%edx, %edi
	call	*%rax
	movl	%eax, %esi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -4(%rbp)
.L14:
	movl	-4(%rbp), %eax
	cmpl	-28(%rbp), %eax
	jle	.L15
	movl	$.LC1, %edi
	call	puts
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4:
	.size	print_n, .-print_n
	.globl	nanodifftime
	.type	nanodifftime, @function
nanodifftime:
.LFB5:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	subq	%rax, %rdx
	movq	%rdx, %rax
	imulq	$1000000000, %rax, %rdx
	movq	-16(%rbp), %rax
	movq	8(%rax), %rcx
	movq	-8(%rbp), %rax
	movq	8(%rax), %rax
	subq	%rax, %rcx
	movq	%rcx, %rax
	addq	%rdx, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5:
	.size	nanodifftime, .-nanodifftime
	.section	.rodata
.LC2:
	.string	"%d\t%d\n"
	.text
	.globl	time_n
	.type	time_n, @function
time_n:
.LFB6:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%rdi, -72(%rbp)
	movl	%esi, -76(%rbp)
	movq	-72(%rbp), %rax
	movl	$1, %edi
	call	*%rax
	movl	$func, %edi
	call	puts
	movq	$0, -8(%rbp)
	movl	$20, -20(%rbp)
	movl	$1, -12(%rbp)
	jmp	.L19
.L22:
	movl	$0, -16(%rbp)
	jmp	.L20
.L21:
	leaq	-48(%rbp), %rax
	movq	%rax, %rsi
	movl	$0, %edi
	call	clock_gettime
	movl	-12(%rbp), %edx
	movq	-72(%rbp), %rax
	movl	%edx, %edi
	call	*%rax
	leaq	-64(%rbp), %rax
	movq	%rax, %rsi
	movl	$0, %edi
	call	clock_gettime
	leaq	-64(%rbp), %rdx
	leaq	-48(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	nanodifftime
	addq	%rax, -8(%rbp)
	addl	$1, -16(%rbp)
.L20:
	movl	-16(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jl	.L21
	movl	-20(%rbp), %eax
	movslq	%eax, %rcx
	movq	-8(%rbp), %rax
	cqto
	idivq	%rcx
	movq	%rax, %rdx
	movl	-12(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -12(%rbp)
.L19:
	movl	-12(%rbp), %eax
	cmpl	-76(%rbp), %eax
	jle	.L22
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6:
	.size	time_n, .-time_n
	.globl	pascal
	.type	pascal, @function
pascal:
.LFB7:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movl	%edi, -20(%rbp)
	movl	%esi, -24(%rbp)
	cmpl	$1, -20(%rbp)
	jle	.L24
	cmpl	$0, -24(%rbp)
	je	.L24
	movl	-24(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jne	.L25
.L24:
	movl	$1, %eax
	jmp	.L26
.L25:
	movl	-20(%rbp), %eax
	leal	-1(%rax), %edx
	movl	-24(%rbp), %eax
	movl	%eax, %esi
	movl	%edx, %edi
	call	pascal
	movl	%eax, %ebx
	movl	-24(%rbp), %eax
	leal	-1(%rax), %edx
	movl	-20(%rbp), %eax
	subl	$1, %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	pascal
	addl	%ebx, %eax
.L26:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	pascal, .-pascal
	.globl	pascalrow
	.type	pascalrow, @function
pascalrow:
.LFB8:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movl	%edi, -52(%rbp)
	movq	%rsp, %rax
	movq	%rax, %rbx
	movl	-52(%rbp), %eax
	addl	$1, %eax
	movslq	%eax, %rsi
	subq	$1, %rsi
	movq	%rsi, -32(%rbp)
	movslq	%eax, %rsi
	movq	%rsi, %r8
	movl	$0, %r9d
	movslq	%eax, %rsi
	movq	%rsi, %rdx
	movl	$0, %ecx
	cltq
	salq	$2, %rax
	leaq	3(%rax), %rdx
	movl	$16, %eax
	subq	$1, %rax
	addq	%rdx, %rax
	movl	$16, %edi
	movl	$0, %edx
	divq	%rdi
	imulq	$16, %rax, %rax
	subq	%rax, %rsp
	movq	%rsp, %rax
	addq	$3, %rax
	shrq	$2, %rax
	salq	$2, %rax
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, -48(%rbp)
	movl	$0, -20(%rbp)
	jmp	.L28
.L29:
	movl	-20(%rbp), %edx
	movl	-52(%rbp), %eax
	movl	%edx, %esi
	movl	%eax, %edi
	call	pascal
	movq	-40(%rbp), %rdx
	movl	-20(%rbp), %ecx
	movslq	%ecx, %rcx
	movl	%eax, (%rdx,%rcx,4)
	addl	$1, -20(%rbp)
.L28:
	movl	-20(%rbp), %eax
	cmpl	-52(%rbp), %eax
	jle	.L29
	movq	-48(%rbp), %rax
	movq	%rbx, %rsp
	movq	-8(%rbp), %rbx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8:
	.size	pascalrow, .-pascalrow
	.section	.rodata
.LC3:
	.string	""
	.text
	.globl	main
	.type	main, @function
main:
.LFB9:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movq	%rsi, -32(%rbp)
	movl	$5, -16(%rbp)
	movl	-16(%rbp), %eax
	movl	%eax, %esi
	movl	$f_recursive, %edi
	call	print_n
	movl	-16(%rbp), %eax
	movl	%eax, %esi
	movl	$f_tailrec, %edi
	call	print_n
	movl	-16(%rbp), %eax
	movl	%eax, %esi
	movl	$f_looping, %edi
	call	print_n
	movl	$30, -16(%rbp)
	movl	-16(%rbp), %eax
	movl	%eax, %esi
	movl	$f_recursive, %edi
	call	time_n
	movl	$500, -16(%rbp)
	movl	-16(%rbp), %eax
	movl	%eax, %esi
	movl	$f_tailrec, %edi
	call	time_n
	movl	-16(%rbp), %eax
	movl	%eax, %esi
	movl	$f_looping, %edi
	call	time_n
	movl	$5, -16(%rbp)
	movl	-16(%rbp), %eax
	movl	%eax, %edi
	call	pascalrow
	movq	%rax, -8(%rbp)
	movl	$0, -12(%rbp)
	jmp	.L32
.L33:
	movq	-8(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	%rdx, -8(%rbp)
	movl	(%rax), %eax
	movl	%eax, %esi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -12(%rbp)
.L32:
	movl	-16(%rbp), %eax
	addl	$1, %eax
	cmpl	-12(%rbp), %eax
	jg	.L33
	movl	$.LC3, %edi
	call	puts
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9:
	.size	main, .-main
	.ident	"GCC: (GNU) 4.8.1 20130725 (prerelease)"
	.section	.note.GNU-stack,"",@progbits
