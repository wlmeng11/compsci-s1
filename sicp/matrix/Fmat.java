public class Fmat {
	/**
	 * A 2-dimenstional matrix, with matrix multiplication
	 */
	static class Matrix {
		int arr[][];

		/**
		 * Construct zero matrix
		 */
		Matrix(int rows, int cols) {
			arr = new int[rows][cols];
		}
		/**
		 * Construct a matrix from the given 2D array
		 */
		Matrix(int arr[][]) {
			this.arr = arr;
		}
		/**
		 * Accessor methods for matrix height & width
		 */
		int rows() { return arr.length; }
		int cols() { return arr[0].length; } // we're assuming that every row has the same length...

		/**
		 * Matrix multiplication: (this)*(that) = (out)
		 */
		Matrix mult(Matrix that) {
			// Assertion: this.cols = that.rows
			if (this.cols() != that.rows())
				return null;

			// Invariant: out.rows = this.rows, out.cols = that.cols
			Matrix out = new Matrix(this.rows(), that.cols());
			for (int i = 0; i < out.rows(); i++) { // aka this.rows
				for (int j = 0; j < out.cols(); j++) { // aka that.cols
					int dp = 0; // dot product
					for (int k = 0; k < that.rows(); k++) {
						int a = this.arr[i][k]; // temporary vars used to simplify debugging
						int b = that.arr[k][j]; // can be rewritten to one line
						dp += a*b;
					}
					out.arr[i][j] = dp;
				}
			}

			return out;
		}

		/**
		 * Get matrix element at the given row and column
		 */
		int get(int row, int col) {
			return arr[row][col];
		}
	}

	/**
	 * Exercise 1.11:
	 * Implementation through matrix exponentiation.
	 * Should be more efficient than other implementations for large n,
	 * more so once I implement exponentiation through squaring.
	 */
	static int f_mat(int n) {
		if (n < 3) // base case
			return n;
		int k = n/2; // k = floor(n/2)

		int arr[][] = { {0}, {1}, {2} };
		Matrix anchor = new Matrix(arr); // base case matrix
		//System.out.println("rows: " + anchor.rows());
		//System.out.println("cols: " + anchor.cols());

		arr = new int[][] { {0, 0, 1}, {3, 2, 1}, {3, 5, 3} };
		Matrix it = new Matrix(arr); // matrix to multiply by at each iteration

		Matrix out = it; // output matrix

		for (int i = 1; i < k; i++) {
			out = out.mult(it);
			// need to use exponentiation through squaring for efficiency
		}
		out = out.mult(anchor); // multiply by base case last

		// (it)^k * (anchor) = out
		if (n%2 == 0) // even
			return out.get(0, 0); // a_{2k}
		else // odd
			return out.get(1, 0); // a_{2k+1}
	}

	public static void main(String[] args) {
		// print first 10 elements
		System.out.println("Elements starting from f(0):");
		int max = 20; // problem: going too high exceeds max integer size, resulting in garbage values
		for (int i = 0; i < max; i++) {
			System.out.println(f_mat(i));
		}

		// Disregard below (test code for Matrix multiplication);
		int arr[][] = { {1, 2}, {3, 4} };
		Matrix a = new Matrix(arr);
		arr = new int[][] { {1, 0}, {0, 1} }; // identity matrix
		Matrix id = new Matrix(arr);

		a.mult(id);
	}
}
