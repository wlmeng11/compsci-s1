import java.util.Arrays;

class Exercises {
	/** Exercise 11.11:
	*/
	static int f_recursive(int n) {
		if (n < 3)
			return n;
		else
			return f_recursive(n-1) + 2*f_recursive(n-2) + 3*f_recursive(n-3);
	}

	static int f_tailrec(int n) {
		return f_iter(2, 1, 0, n);
	}
	/**
	 * Private helper method because Java doesn't have nested functions
	 * @param a			f(n+2)
	 * @param b			f(n+1)
	 * @param c			f(n)
	 * @param counter	Counts down until 0, represents n
	 */
	private static int f_iter(int a, int b, int c, int counter) {
		if (counter == 0)
			return c;
		else
			return f_iter(a + 2*b + 3*c, a, b, counter - 1);
	}

	static int f_looping(int n) {
		int a = 2; // f(n+2)
		int b = 1; // f(n+1)
		int c = 0; // f(n)
		for (int i = 0; i < n; i++) {
			int new_a = a + 2*b + 3*c;
			c = b;
			b = a;
			a = new_a;
		}
		return c;
	}

	/**
	 * Java's horrible alternative to function pointers: passing objects of classes implementing a common interface!
	 */
	interface Runnable {
		int execute(int n);
	}
	static class F_Recursive implements Runnable {
		public int execute(int n) { return f_recursive(n); }
	}
	static class F_Tailrec implements Runnable {
		public int execute(int n) { return f_tailrec(n); }
	}
	static class F_Looping implements Runnable {
		public int execute(int n) { return f_looping(n); }
	}
	/* Prints the first n results of a function f */
	static void print_n(Runnable f, int n) {
		System.out.println(f.getClass().getName());
		for (int i = 1; i <= n; i++)
			System.out.print(f.execute(i) + " ");
		System.out.println("\n");
	}
	/* Print runtime, in nanoseconds, for each function call with increasing i to n */
	static void time_n(Runnable f, int n) {
		System.out.println(f.getClass().getName());
		long start, end;
		int trials = 20;
		long sum = 0;
		for (int i = 1; i <= n; i++) {
			for (int j = 0; j < trials; j++) { // do multiple trials and average
				start = System.nanoTime();
				f.execute(i);
				end = System.nanoTime();
				sum += end-start;
			}
			String s = String.format("%d\t%d", i, sum/trials);
			System.out.println(s);
		}
	}

	public static void main(String[] args) {
		System.out.println("Exercise 11.11:");
		int n = 10;
		print_n(new F_Recursive(), n);
		print_n(new F_Tailrec(), n);
		print_n(new F_Looping(), n);

		/* Test orders of growth */
		n = 30;
		time_n(new F_Recursive(), n);
		n = 500;
		time_n(new F_Tailrec(), n);
		time_n(new F_Looping(), n);

		System.out.println();
		System.out.println("Exercise 11.12:");
		System.out.println("Element at (4, 3): " + pascal(4, 3));
		int[] row = pascalrow(5);
		System.out.println("Row 5: " + Arrays.toString(row));
	}

	/** Exercise 11.12:
	 * Returns the element of Pascal's triangle
	 * at the nth row and rth column, counting with
	 * a zero index.
	 *
	 * Note that this is mathematically identical to nCr,
	 * read "n choose r".
	 */
	static int pascal(int n, int r) {
		if (n == 0 && r == 0)
			return 1; // base case: element (0, 0)
		else if (n < 0 || r < 0 || r > n)
			return 0; // fill outside area with zeros
		else
			return pascal(n-1, r) + pascal(n-1, r-1);
	}

	static int[] pascalrow(int n) {
		int[] row = new int[n+1];
		for (int r = 0; r <= n; r++)
			row[r] = pascal(n, r);
		return row;
	}
}
