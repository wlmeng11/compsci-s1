	.file	"Exercises.c"
	.text
	.p2align 4,,15
	.globl	f_recursive
	.type	f_recursive, @function
f_recursive:
.LFB25:
	.cfi_startproc
	pushq	%r13
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	movabsq	$8318840531470933862, %rax
	pushq	%r12
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbp
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	movl	$1, %ebp
	pushq	%rbx
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	movl	%edi, %ebx
	subq	$8, %rsp
	.cfi_def_cfa_offset 48
	cmpl	$2, %edi
	movq	%rax, func(%rip)
	movl	$6649449, func+8(%rip)
	jle	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	leal	-1(%rbx), %edi
	call	f_recursive
	leal	-2(%rbx), %edi
	movl	%eax, %r13d
	subl	$3, %ebx
	call	f_recursive
	leal	0(%r13,%rax,2), %eax
	movl	$6649449, func+8(%rip)
	imull	%ebp, %eax
	leal	0(%rbp,%rbp,2), %ebp
	addl	%eax, %r12d
	movabsq	$8318840531470933862, %rax
	cmpl	$2, %ebx
	movq	%rax, func(%rip)
	jg	.L3
.L2:
	imull	%ebp, %ebx
	addq	$8, %rsp
	.cfi_def_cfa_offset 40
	leal	(%r12,%rbx), %eax
	popq	%rbx
	.cfi_def_cfa_offset 32
	popq	%rbp
	.cfi_def_cfa_offset 24
	popq	%r12
	.cfi_def_cfa_offset 16
	popq	%r13
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE25:
	.size	f_recursive, .-f_recursive
	.p2align 4,,15
	.globl	f_tailrec
	.type	f_tailrec, @function
f_tailrec:
.LFB26:
	.cfi_startproc
	movabsq	$7310024345038118758, %rax
	testl	%edi, %edi
	movq	%rax, func(%rip)
	movl	$99, %eax
	movw	%ax, func+8(%rip)
	je	.L10
	xorl	%edx, %edx
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%eax, %edx
	movl	%ecx, %eax
	movl	%esi, %ecx
.L9:
	leal	(%rcx,%rax,2), %esi
	leal	(%rdx,%rdx,2), %edx
	addl	%edx, %esi
	subl	$1, %edi
	jne	.L14
	rep ret
.L10:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE26:
	.size	f_tailrec, .-f_tailrec
	.p2align 4,,15
	.globl	f_looping
	.type	f_looping, @function
f_looping:
.LFB28:
	.cfi_startproc
	movabsq	$7956013840639745894, %rax
	movl	$103, %edx
	testl	%edi, %edi
	movq	%rax, func(%rip)
	movw	%dx, func+8(%rip)
	jle	.L18
	xorl	%edx, %edx
	movl	$1, %eax
	movl	$2, %ecx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L22:
	movl	%eax, %edx
	movl	%ecx, %eax
	movl	%esi, %ecx
.L17:
	leal	(%rcx,%rax,2), %esi
	leal	(%rdx,%rdx,2), %edx
	addl	%edx, %esi
	subl	$1, %edi
	jne	.L22
	rep ret
.L18:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE28:
	.size	f_looping, .-f_looping
	.p2align 4,,15
	.globl	f_iter
	.type	f_iter, @function
f_iter:
.LFB27:
	.cfi_startproc
	testl	%ecx, %ecx
	movl	%edx, %eax
	jne	.L25
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L30:
	movl	%edi, %esi
	movl	%edx, %edi
.L25:
	leal	(%rax,%rax,2), %eax
	leal	(%rdi,%rsi,2), %edx
	addl	%eax, %edx
	subl	$1, %ecx
	movl	%esi, %eax
	jne	.L30
.L24:
	rep ret
	.cfi_endproc
.LFE27:
	.size	f_iter, .-f_iter
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"\n"
.LC1:
	.string	"%d "
	.text
	.p2align 4,,15
	.globl	print_n
	.type	print_n, @function
print_n:
.LFB29:
	.cfi_startproc
	pushq	%r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	movq	%rdi, %r12
	movl	$1, %edi
	pushq	%rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movl	%esi, %ebp
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movl	$1, %ebx
	call	*%r12
	movl	$func, %edi
	call	puts
	testl	%ebp, %ebp
	jle	.L33
	.p2align 4,,10
	.p2align 3
.L35:
	movl	%ebx, %edi
	addl	$1, %ebx
	call	*%r12
	movl	$.LC1, %edi
	movl	%eax, %esi
	xorl	%eax, %eax
	call	printf
	cmpl	%ebx, %ebp
	jge	.L35
.L33:
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%rbp
	.cfi_def_cfa_offset 16
	popq	%r12
	.cfi_def_cfa_offset 8
	movl	$.LC0, %edi
	jmp	puts
	.cfi_endproc
.LFE29:
	.size	print_n, .-print_n
	.p2align 4,,15
	.globl	nanodifftime
	.type	nanodifftime, @function
nanodifftime:
.LFB30:
	.cfi_startproc
	movq	(%rsi), %rax
	subq	(%rdi), %rax
	imulq	$1000000000, %rax, %rax
	addq	8(%rsi), %rax
	subq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE30:
	.size	nanodifftime, .-nanodifftime
	.section	.rodata.str1.1
.LC2:
	.string	"%d\t%d\n"
	.text
	.p2align 4,,15
	.globl	time_n
	.type	time_n, @function
time_n:
.LFB31:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$1, %edi
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	call	*%r13
	movl	$func, %edi
	call	puts
	testl	%r14d, %r14d
	jle	.L38
	movl	$1, %r12d
	xorl	%ebp, %ebp
	movabsq	$7378697629483820647, %r15
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$20, %ebx
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rsp, %rsi
	xorl	%edi, %edi
	call	clock_gettime
	movl	%r12d, %edi
	call	*%r13
	leaq	16(%rsp), %rsi
	xorl	%edi, %edi
	call	clock_gettime
	movq	16(%rsp), %rax
	subq	(%rsp), %rax
	imulq	$1000000000, %rax, %rax
	addq	24(%rsp), %rax
	subq	8(%rsp), %rax
	addq	%rax, %rbp
	subl	$1, %ebx
	jne	.L42
	movq	%rbp, %rax
	movl	%r12d, %esi
	movl	$.LC2, %edi
	imulq	%r15
	movq	%rbp, %rax
	addl	$1, %r12d
	sarq	$63, %rax
	sarq	$3, %rdx
	subq	%rax, %rdx
	xorl	%eax, %eax
	call	printf
	cmpl	%r12d, %r14d
	jge	.L40
.L38:
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE31:
	.size	time_n, .-time_n
	.p2align 4,,15
	.globl	pascal
	.type	pascal, @function
pascal:
.LFB32:
	.cfi_startproc
	pushq	%r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	cmpl	$1, %edi
	pushq	%rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movl	%edi, %ebp
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	jle	.L51
	testl	%esi, %esi
	movl	%esi, %ebx
	je	.L51
	cmpl	%esi, %edi
	je	.L51
	xorl	%r12d, %r12d
	.p2align 4,,2
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L54:
	cmpl	$1, %ebp
	jle	.L52
	cmpl	%ebx, %ebp
	.p2align 4,,3
	je	.L52
.L46:
	subl	$1, %ebp
	movl	%ebx, %esi
	movl	%ebp, %edi
	call	pascal
	addl	%eax, %r12d
	subl	$1, %ebx
	jne	.L54
.L52:
	leal	1(%r12), %eax
.L45:
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbp
	.cfi_def_cfa_offset 16
	popq	%r12
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L45
	.cfi_endproc
.LFE32:
	.size	pascal, .-pascal
	.p2align 4,,15
	.globl	pascalrow
	.type	pascalrow, @function
pascalrow:
.LFB33:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	1(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	cltq
	leaq	18(,%rax,4), %rax
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edi, %r12d
	andq	$-16, %rax
	pushq	%rbx
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	subq	%rax, %rsp
	testl	%edi, %edi
	movq	%rsp, %r13
	js	.L59
	.p2align 4,,10
	.p2align 3
.L60:
	movl	%ebx, %esi
	movl	%r12d, %edi
	call	pascal
	movslq	%ebx, %rdx
	addl	$1, %ebx
	cmpl	%ebx, %r12d
	movl	%eax, 0(%r13,%rdx,4)
	jge	.L60
.L59:
	leaq	-24(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE33:
	.size	pascalrow, .-pascalrow
	.section	.rodata.str1.1
.LC3:
	.string	""
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB34:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %esi
	movl	$f_recursive, %edi
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	subq	$8, %rsp
	.cfi_def_cfa_offset 32
	call	print_n
	movl	$5, %esi
	movl	$f_tailrec, %edi
	call	print_n
	movl	$5, %esi
	movl	$f_looping, %edi
	call	print_n
	movl	$30, %esi
	movl	$f_recursive, %edi
	call	time_n
	movl	$500, %esi
	movl	$f_tailrec, %edi
	call	time_n
	movl	$500, %esi
	movl	$f_looping, %edi
	call	time_n
	movl	$5, %edi
	call	pascalrow
	leaq	24(%rax), %rbp
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L64:
	addq	$4, %rbx
	movl	-4(%rbx), %esi
	xorl	%eax, %eax
	movl	$.LC1, %edi
	call	printf
	cmpq	%rbp, %rbx
	jne	.L64
	movl	$.LC3, %edi
	call	puts
	addq	$8, %rsp
	.cfi_def_cfa_offset 24
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE34:
	.size	main, .-main
	.comm	func,80,32
	.ident	"GCC: (GNU) 4.8.1 20130725 (prerelease)"
	.section	.note.GNU-stack,"",@progbits
