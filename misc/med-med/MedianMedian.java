/**
 * @author William Meng
 */
import java.util.*;

/**
 * This class calculates the median-median line for a set of points.
 */
public class MedianMedian {
	/**
	  A double-precision Point class
	  Conflicts with java.awt.Point, so don't import it
	  */
	static class Point {
		double x;
		double y;

		public Point(double x, double y) {
			this.x = x;
			this.y = y;
		}
	}

	/* UTILITY functions */
	/**
	  Sorts a list of points by ascending x-coordinates
	  */
	public static void sortByX(List<Point> points) {
		Comparator<Point> comp = new Comparator<Point>() {
			public int compare(Point a, Point b) {
				return new Double(a.x).compareTo(b.x);
			}
		};
		Collections.sort(points, comp);
	}

	/**
	  Sorts a list of points by ascending y-coordinates
	  */
	public static void sortByY(List<Point> points) {
		Comparator<Point> comp = new Comparator<Point>() {
			public int compare(Point a, Point b) {
				return new Double(a.y).compareTo(b.y);
			}
		};
		Collections.sort(points, comp);
	}

	/**
	 * Returns a point with x-coordinate that is the mean of the
	 * x-coordinates, and y-coordinate that is the mean of the
	 * y-coordinates
	 */
	public static Point meanPoint(List<Point> points) {
		double xSum = 0;
		double ySum = 0;
		for (Point pt : points) {
			xSum += pt.x;
			ySum += pt.y;
		}
		return new Point(xSum/points.size(), ySum/points.size());
	}

	/**
	 * Returns a point with x-coordinate that is the median of
	 * all the x-coordinates, and y-coordinate that is the median
	 * of all the y-coordinates
	 */
	public static Point medianPoint(List<Point> points) {
		double x = 0, y = 0;
		List<Point> yOrdered = new ArrayList<Point>(points);
		sortByY(yOrdered);
		int half = points.size()/2;

		if (points.size()%2 == 1) {
			x = points.get(half+1).x;
			y = points.get(half+1).y;
		}
		else {
			x = (points.get(half).x + points.get(half+1).x)/2;
			y = (points.get(half).y + points.get(half+1).y)/2;
		}

		return new Point(x, y);
	}

	/**
	  Returns the slope between two points
	  */
	public static double getSlope(Point pt1, Point pt2) {
		return (pt2.y - pt1.y)/(pt2.x - pt1.x);
	}

	/**
	  Returns the y-intercept of a line with given slope,
	  passing through a given line.
	  */
	public static double slopeIntercept(Point pt, double slope) {
		return pt.y - slope*pt.x;
	}

	/**
	 * Prints out a list of points
	 */
	public static void printPoints(List<Point> points) {
		for (Point pt : points)
			System.out.println("(" + pt.x + ", " + pt.y + ")");
	}
	/* end UTILITY functions */

	List<Point> points;
	List<Point>	group1, group2, group3;
	Point m1, m2, m3; // median point for each group
	double slope;
	double b1, b2; // y-intercept of line through each median point
	double bFinal; // y-intercept of final line

	MedianMedian(List<Point> points) {
		this.points = points;
		List<Point> group1 = new ArrayList<Point>();
		List<Point> group2 = new ArrayList<Point>();
		List<Point> group3 = new ArrayList<Point>();

		init();
	}

	/**
	 * Handle all the method-calling and printing
	 */
	private void init() {
		sortByX(points);
		makeGroups();

		m1 = medianPoint(group1);
		m2 = medianPoint(group2);
		m3 = medianPoint(group3);

		slope = getSlope(m1, m3);

		b1 = slopeIntercept(m1, slope);
		b2 = slopeIntercept(m2, slope);
		bFinal = (b1 + b1 + b2)/3;
	}

	public void print() {
		printPoints(points);
		System.out.println("Group 1:");
		printPoints(group1);
		System.out.println("Group 2:");
		printPoints(group2);
		System.out.println("Group 3:");
		printPoints(group3);
		String string = String.format("M1: (%f, %f)\nM2: (%f, %f)\nM3: (%f, %f)",
				m1.x, m1.y, m2.x, m2.y, m3.x, m3.y);
		System.out.println(string);
		System.out.println("Slope: " + slope);

		System.out.println("Equation: y = " + slope + "x + " + bFinal);
	}

	/**
	 * Splits points into 3 groups
	 */
	private void makeGroups() {
		int third = points.size()/3;

		switch (points.size()%3) {
			case 0:
				//System.out.println("split evenly");
				group1 = points.subList(0, third);
				group2 = points.subList(third, third*2);
				group3 = points.subList(third*2, points.size());
				break;
			case 1:
				//System.out.println("group 2 is bigger");
				group1 = points.subList(0, third);
				group2 = points.subList(third, third*2+1);
				group3 = points.subList(third*2+1, points.size());
				break;
			case 2:
				//System.out.println("group 2 is smaller");
				group1 = points.subList(0, third+1);
				group2 = points.subList(third+1, third*2+1);
				group3 = points.subList(third*2+1, points.size());
				break;
		}
	}

	public static void main(String[] args) {
		List<Point> points = new ArrayList<Point>();
		points.add(new Point(1.0, 2.0));
		points.add(new Point(0.5, 3.0));
		points.add(new Point(0.7, 1.0));
		points.add(new Point(5.1, 2.3));
		points.add(new Point(0.7, 1.2));
		points.add(new Point(0.7, 1.2));
		points.add(new Point(0.7, 1.3));
		points.add(new Point(0.8, 1.3));
		points.add(new Point(0.9, 1.4));

		MedianMedian m = new MedianMedian(points);
		m.print();
	}
}
