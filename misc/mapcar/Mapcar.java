/**
 * A mapcar implementation for integer array types.
 * @author William Meng
 */
import java.util.*;

public class Mapcar {
	/**
	 * Specifies a function, called run, with a variable number of integer arguments (varargs).
	 */
	public interface Function {
		public int run(int... list);
	}

	/**
	 * Returns the sum of the given integers.
	 */
	public static final class Add implements Function {
		public int run(int... list) {
			int sum = 0;
			for (int i : list) {
				sum += i;
			}
			return sum;
		}
	}

	/**
	 * Returns the product of the given integers.
	 */
	public static final class Mult implements Function {
		public int run(int... list) {
			int product = 1;
			for (int i : list) {
				product *= i;
			}
			return product;
		}
	}

	/**
	 * Raises list[0] to the power of list[1],
	 * then raises the result to the power of list[2], etc.
	 */
	public static final class Exp implements Function {
		/**
		 * Helper method for exponentiation
		 */
		private int exp(int base, int power) {
			int ret = 1;
			for (int i = 0; i < power; i++) {
				ret *= base;
			}
			return ret;
		}

		public int run(int... list) {
			int ret = list[0];
			for (int i = 1; i < list.length; i++) {
				ret = exp(ret, list[i]);
			}
			return ret;
		}
	}

	/**
	 * Given a function and at least one integer array,
	 * the element at each index of the ouput array is the result
	 * of passing the element of each array at that index
	 * to the given function.
	 *
	 * The output array is as long as the shortest given array.
	 *
	 * @param f 		function to apply
	 * @param lists 	varargs of type int[]
	 */
	public static ArrayList<Integer> mapcar(Function f, ArrayList<Integer>... lists) {
		// find length of shortest list
		int length = lists[0].size();
		for (ArrayList<Integer> list : lists) {
			if (list.size() < length)
				length = list.size();
		}

		ArrayList<Integer> ret = new ArrayList<>();
		ret.ensureCapacity(length);

		// loop for index
		for (int i = 0; i < length; i++) {
			int[] par = new int[lists.length];

			// loop for element of each list at the index
			int j = 0;
			for (ArrayList<Integer> list : lists) {
				par[j] = list.get(i);
				j++;
			}

			ret.add(f.run(par));
		}

		return ret;
	}

	public static void main(String[] args) {
		final int[] foo = {1, 2, 3, 4, 5};
		final int[] bar = {2, 4, 6};
		final int[] fiz = {0, 3, 2, 4};
		final int[] baz = {4, 1, 5, 3, 6};

		System.out.println("Adding foo and bar");
		ArrayList<Integer> out = mapcar(new Add(), toArrList(foo), toArrList(bar));
		printArrList(out);
		// Should print:
		// [3, 6, 9]

		System.out.println("Multiplying foo and fiz");
		out = mapcar(new Mult(), toArrList(foo), toArrList(fiz));
		printArrList(out);
		// Should print:
		// [0, 6, 6, 16]

		System.out.println("Exponentiating fiz to baz");
		// fiz to the baz power, for each corresponding element
		out = mapcar(new Exp(), toArrList(fiz), toArrList(baz));
		printArrList(out);
		// Should print:
		// [0, 3, 32, 64]

		System.out.println("Exponentiating fiz to baz to bar");
		// fiz to the baz to the bar power, for each corresponding element
		out = mapcar(new Exp(), toArrList(fiz), toArrList(baz), toArrList(bar));
		printArrList(out);
		// Should print:
		// [0, 81, 1073741824]
	}

	/* Returns an ArrayList<Integer> with the elements of a given int[] */
	public final static ArrayList<Integer> toArrList(int[] arr) {
		ArrayList<Integer> list = new ArrayList<>();
		for (int i : arr) {
			list.add(i);
		}
		return list;
	}

	/** Prints an ArrayList<Integer> */
	public final static void printArrList(ArrayList<Integer> arr) {
		System.out.print("[");
		try {
			for (int i = 0; i < arr.size()-1; i++) {
				System.out.print(arr.get(i) + ", ");
			}
			System.out.print(arr.get(arr.size()-1));
		} catch (IndexOutOfBoundsException e) {
		}
		System.out.println("]");
	}
}
