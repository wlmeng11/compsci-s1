class Foo {
	int x, y;

	Foo() {
		x = 1;
		y = 2;
	}

	void print() {
		System.out.println(x + "," + y);
	}
}
