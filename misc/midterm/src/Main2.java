class Main2 {
	static void changeFoo(Foo f) {
		f.x = 10;
		f.y = 11;
	}

	public static void main(String[] args) {
		Foo f = new Foo();
		f.x = 3;
		f.y = 3;

		changeFoo(f);
		f.print();
	}
}
