class Main1 {
	static void resetFoo(Foo f) {
		f = new Foo();
	}

	public static void main(String[] args) {
		Foo f = new Foo();
		f.x = 3;
		f.y = 3;

		resetFoo(f);
		f.print();
	}
}
