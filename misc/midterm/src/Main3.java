class Main3 {
	static void setToNewFoo(Foo f) {
		Foo n = new Foo();
		f.x = n.x;
		f.y = n.y;
	}

	public static void main(String[] args) {
		Foo f = new Foo();
		f.x = 3;
		f.y = 3;

		setToNewFoo(f);
		f.print();
	}
}
