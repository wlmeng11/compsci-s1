/**
 * Find the median value of an array in O(n) time,
 * utilizing the quickselect algorithm, which is based off of quicksort.
 *
 * @author William Meng
 */
import java.util.Arrays;

public class Quicksort {
	/* A quicksort, for reference. */
	public static int[] quicksort(int[] arr) {
		int[] list = arr.clone();
		quicksort(list, 0, list.length-1);
		return list;
	}
	/* Helper method for quicksort */
	public static void quicksort(int[] list, int first, int last) {
		if (last > first) {
			int p = partition(list, first, last);
			quicksort(list, first, p-1);
			quicksort(list, p+1, last);
		}
	}

	/**
	 * In-place partition of list[first...last]:
	 * move elements smaller than pivot value to in front of it,
	 * larger elements to end
	 * Return pivot position
	 * O(n) time
	 */
	private static int partition(int[] list, int first, int last) {
		int pivot = list[last];

		// invariant: outer loop is O(n)
		while (first < last) {
			/* these two loops only advance partially on each iteration of outer loop,
			 * so in the end, together they only make O(n) increments
			 */
			while (list[first] < pivot)
				first++;
			while (list[last] > pivot)
				last--;

			// constant time
			if (list[first] == list[last])
				first++;
			else if (first < last) {
				int tmp = list[first];
				list[first] = list[last];
				list[last] = tmp;
			}
		}
		return last;
	}

	/**
	 * Finds the median of an array in O(n) time
	 * O(n) + O(1) + O(n) + O(n) + O(1)
	 * = O(n)
	 */
	public static int median(int[] arr) {
		int[] list = arr.clone(); /* O(n) time for malloc & copying
									 Not actually necessary
									 I did this so the method is non-destructive on input.
									 */
		// this block is O(1)
		int first = 0;
		int last = list.length-1;
		// Determine parity of array length
		boolean odd = true;
		if (list.length % 2 == 0)
			odd = false;

		// find n/2 ordered element
		int medleft = quickselect(list, first, last, list.length/2); // takes O(n) time
		// find n/2 + 1 ordered element
		int medright = quickselect(list, first, last, list.length/2 + 1); // also takes O(n) time

		// O(1) for rest
		if (odd)
			return medright;
		else
			return (medleft+medright)/2;
	}

	/**
	 * Return the nth ordered element, in 1-indexing, of the array
	 *
	 * Recurrence relation is given by:
	 * T(1) = O(1),
	 * T(n) = n + T(n/2)
	 *      = n + n/2 + T(n/4)
	 *      = n + n/2 + n/4 + T(n/8)
	 *      = n + n/2 + n/4 + n/8 + ...
	 *      = n + 1
	 *      = O(n)
	 * Therefore, O(n) time complexity
	 */
	public static int quickselect(int[] list, int first, int last, int n) {
		if (first == last) // sublist is only one element, so return that
			return list[first]; // T(1) = O(1)

		int p = partition(list, first, last); // O(n)
		// pivot is now in sorted position
		int dist = p - first + 1;
		if (dist == n)
			return list[p];
		// split into a single smaller problem, since only one sublist is of interest,
		// rather than both in quicksort
		else if (n < dist)
			return quickselect(list, first, p-1, n); // T(n/2)
		else
			return quickselect(list, p+1, last, n-dist); // T(n/2)
	}

	public static void main(String[] args) {
		int[] arr = new int[] {254, 1868, 148, 1678, 100, 192, 115, 118, 971, 8};
		System.out.println("Original array:\n" + Arrays.toString(arr));
		System.out.println("Median: " + median(arr));

		System.out.println();
		System.out.println("Quick sorted array (for validation):");
		System.out.println(Arrays.toString(quicksort(arr)));

		System.out.println("\nMore arrays!\nFirst original array is printed, then median, then sorted array for validation"); 

		int[] a = {4, 43, 72, 35, 2, 957, 45};
		System.out.println("a:\n" + Arrays.toString(a));
		System.out.println("med of a: " + median(a));
		System.out.println(Arrays.toString(quicksort(a)));

		System.out.println();
		int[] b = {394, 498, 131, 157, 127, 365, 113, 377, 230, 248, 348, 318, 359, 461, 402, 42};
		System.out.println("b:\n" + Arrays.toString(b));
		System.out.println("med of b: " + median(b));
		System.out.println(Arrays.toString(quicksort(b)));
	}
}
