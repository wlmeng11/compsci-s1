/**
  Several sort algorithms, implemented for integer arrays
  @author William Meng
  */
import java.util.Arrays;

class Sort {
	/**
	  Non-destructive bubble sort:
	  Favoring efficiency over readability
	  */
	public static int[] bubble(int[] in) {
		int[] arr = in.clone();
		boolean sorted = false;
		// counter variables
		int pass = 0;
		int swaps = 0;

		while (!sorted) { // keep doing passes until sorted
			for (int i = 0; i < arr.length-1; i++) { // loop through all elements (except last
				if (arr[i] > arr[i+1]) { // compare current and next elements
					// XOR swap
					arr[i] ^= arr[i+1];
					arr[i+1] ^= arr[i];
					arr[i] ^= arr[i+1];

					sorted = true; // indicates swap was made (don't want to make another variable)
					swaps++;
					//System.out.println(Arrays.toString(arr)); // optionally print after every swap
				}
			}
			sorted ^= true; /* Toggle the boolean:
							   If a swap was made on this pass (bool is true), then it's not necessarily sorted (set to false).
							   If no swaps were made (bool is false), then it's sorted (set to true).
							   */
			pass++; // count passes
			System.out.println(Arrays.toString(arr));
		}
		System.out.println("Swaps made: " + swaps);
		System.out.println("Number of passes: " + pass);
		return arr;
	}

	/**
	 * Since "inserting" an element doesn't really work in an array,
	 * I just swapped downwards until it was in the right spot.
	 */
	public static int[] insertion(int[] in) {
		int[] arr = in.clone();
		int swaps = 0;
		int insertions = 0;
		for (int i = 1; i < arr.length; i++) { // go through each element, starting from arr[1] because first element can't move down
			boolean inserted = false;
			for (int k = i; k > 0; k--) { // move it down to where it belongs: swapping instead of inserting because it's an array
				if (arr[k-1] > arr[k]) {
					arr[k-1] ^= arr[k]; arr[k] ^= arr[k-1]; arr[k-1] ^= arr[k]; // XOR swap of current and previous element
					swaps++;
					inserted = true;
				}
			}
			if (inserted) insertions++;
			System.out.println(Arrays.toString(arr));
		}
		System.out.println("\"Insertions\"  made: " + insertions);
		System.out.println("Swaps made: " + swaps);
		return arr;
	}

	public static int[] selection(int[] in) {
		int[] arr = in.clone();
		int swaps = 0;
		for (int i = 0; i < arr.length-1; i++) { // i represents front of unsorted list (skip last one, obviously)
			int min = i;
			// find index of smallest element
			for (int k = i; k < arr.length; k++) { // search only in the unsorted list
				if (arr[k] < arr[min])
					min = k;
			}

			// Swap minimum element with front of unsorted list (unless no other min was found)
			if (min != i) {
				arr[min] ^= arr[i]; arr[i] ^= arr[min]; arr[min] ^= arr[i];
				swaps++;
			}
			System.out.println(Arrays.toString(arr));
		}
		System.out.println("Swaps made: " + swaps);
		return arr;
	}

	public static void main(String[] args) {
		//int[] arr = new int[] {5, 235, 2574, 492, 2298, 857, 94, 59};
		int[] arr = new int[] {254, 1868, 148, 1678, 100, 192, 115, 118, 971, 8}; // Do these numbers look famililar? :D
		System.out.println("Original array:\n" + Arrays.toString(arr));

		System.out.println();
		System.out.println("Bubble sort:");
		System.out.println(Arrays.toString(bubble(arr)));

		System.out.println();
		System.out.println("Original array:\n" + Arrays.toString(arr));
		System.out.println("Insertion sort:");
		System.out.println(Arrays.toString(insertion(arr)));

		System.out.println();
		System.out.println("Original array:\n" + Arrays.toString(arr));
		System.out.println("Selection sort:");
		System.out.println(Arrays.toString(selection(arr)));
	}
}
