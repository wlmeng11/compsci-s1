#!/bin/bash
# Overwrites given file with given amount of random numbers

if [ -z "$1" ]; then
	FILE=in.txt # default output file
else
	FILE=$1
fi
echo Using file: $FILE

if [ -z "$2" ]; then
	NUM=20 # default amount
else
	NUM=$2
fi
echo Using amount: $NUM

# remove file if exists
[[ -f $FILE ]] && rm $FILE

for ((i = 0; i < NUM; i++)); do
	echo $RANDOM >> $FILE
done

cat $FILE
